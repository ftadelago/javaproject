package userModel;
/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/

import java.util.Date;
// Start of user code (user defined imports)
import java.util.HashSet;


// End of user code

/**
 * Description of ContrainteHoraire.
 * 
 * @author pc
 */
public class Horaires {
	/**
	 * Description of the property prof.
	 */

	/**
	 * Description of the property sDate.
	 */
	private Date sDate = new Date();

	/**
	 * Description of the property eDate.
	 */
	private Date eDate = new Date();

	/**
	 * Description of the property comment.
	 */
	private String comment = "";

	// Start of user code (user defined attributes for ContrainteHoraire)

	// End of user code

	/**
	 * The constructor.
	 *
	 * @param eDate la date e
	 * @param sDate la date s
	 */
	public Horaires(Date eDate, Date sDate) {
		// Start of user code constructor for ContrainteHoraire)
		this.eDate = eDate;
		this.sDate = sDate;
		// End of user code
	}

	// Start of user code (user defined methods for ContrainteHoraire)

	// End of user code

	/**
	 * Returns sDate.
	 * 
	 * @return sDate
	 */
	public Date getSDate() {
		return this.sDate;
	}

	/**
	 * Sets a value to attribute sDate.
	 *
	 * @param snewDate la nouvelle s date 
	 */
	public void setSDate(Date snewDate) {
		this.sDate = snewDate;
	}

	/**
	 * Returns eDate.
	 * 
	 * @return eDate
	 */
	public Date getEDate() {
		return this.eDate;
	}

	/**
	 * Sets a value to attribute eDate.
	 *
	 * @param enewDate la nouvelle e date
	 */
	public void setEDate(Date enewDate) {
		this.eDate = enewDate;
	}

	/**
	 * Returns comment.
	 * 
	 * @return comment
	 */
	public String getComment() {
		return this.comment;
	}

	/**
	 * Sets a value to attribute comment.
	 *
	 * @param newCom le nouveau commentaire
	 */
	public void setComment(String newCom) {
		this.comment = newCom;
	}

}
