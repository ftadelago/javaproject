package userModel;
/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/

import java.util.Date;
import java.util.HashSet;

// TODO: Auto-generated Javadoc
// End of user code

/**
 * Description of Professeur.
 * 
 * @author pc
 */
public class Professeur extends Utilisateur {
	
	/** The hour. */
	private HashSet<Horaires> time;
	
	
	/**
	 * Description of the property profId.
	 */
	private String profId = "0";

	/**
	 * Instantiates a new professeur.
	 *
	 * @param profLogin le login
	 * @param profId l'identifiant
	 * @param fname le nom
	 * @param sname le surnom
	 * @param password le mdp
	 */
	public Professeur(String login, String profId, String fName, String lName, String password) {
		super(login, password, lName, fName);
		this.profId = profId;
	}

	
	public final String toString() {
		String infos = "Prof:" + super.toString() + " -- " + this.profId;
		return infos;
	}

	
	@Override
	public String check() {
		String checker = "Prof:";
		return checker;
	}

	/**
	 * 
	 * @return profId
	 */
	public String getIDProf() {
		return this.profId;
	}

	/**
	 * Sets a value to attribute profId.
	 *
	 * @param profIdnew l'id du prof
	 */
	public void setIDProf(String profIdnew) {
		this.profId = profIdnew;
	}

	/**
	 * Adds the time.
	 *
	 * @param eDate the e date
	 * @param sDate the s date
	 */
	public void addTime(Date eDate, Date sDate) {
		Horaires h = new Horaires(eDate, sDate);
		time.add(h);
	}

}
