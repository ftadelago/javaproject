package userModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * 
 * Cette classe gére la base de données d'utilisateurs. Elle doit permettre de sauvegarder et charger les utilisateurs et les groupes à partir d'un fichier XML. 
 * La structure du fichier XML devra être la même que celle du fichier userDB.xml.
 * @see <a href="../../userDB.xml">userDB.xml</a> 
 * 
 * @author Jose Mennesson (Mettre à jour)
 * @version 04/2016 (Mettre à jour)
 * 
 */

//TODO Classe à modifier

public class UserDB {
	
	/**
	 * 
	 * Le fichier contenant la base de données.
	 * 
	 */
	private String file;
	
	/**
	 * 
	 * Constructeur de UserDB. 
	 * 
	 * !!!!!!!!!!!! PENSEZ À AJOUTER UN ADMINISTRATEUR (su par exemple) QUI VOUS PERMETTRA DE CHARGER LA BASE DE DONNÉES AU DEMARRAGE DE L'APPLICATION !!!!!!
	 * 
	 * @param file
	 * 		Le nom du fichier qui contient la base de données.
	 */
	private HashMap<String, Groupe> groupes = new HashMap<String, Groupe>();
	private HashMap<String, Utilisateur> UserDBs = new HashMap<String, Utilisateur>();
	
	
	public UserDB(String file){
		//TODO Fonction à modifier
		super();
		this.setFile(file);
	}
	
	/**
	 * Getter de file
	 * 
	 * @return 
	 * 		Le nom du fichier qui contient la base de données.
	 */
	
	public String getFile() {
		return file;
	}
	
	/**
	 * Setter de file
	 * 
	 * @param file
	 * 		Le nom du fichier qui contient la base de données.
	 */
	
	public void setFile(String file) {
		this.file = file;
	}
	public void putUtilisateurs(HashMap<String, Utilisateur> userHash) {
		UserDBs = userHash;
	}
	/**
	 * Sets the users.
	 *
	 * @param Users the users
	 */
	public void setUsers(HashMap<String, Utilisateur> Users) {
		UserDBs = Users;
	}

	/**
	 * Sets the group.
	 *
	 * @param Group the group
	 */
	public void setGroup(HashMap<String, Groupe> Group) {
		groupes = Group;
	}

	/**
	 * Users.
	 *
	 * @return the hash map
	 */
	public HashMap<String, Utilisateur> UserDBHash() {
		return UserDBs;
	}

	/**
	 * Groups.
	 *
	 * @return the hash map
	 */
	public HashMap<String, Groupe> GroupHash() {
		return groupes;
	}

	/**
	 * Convert.
	 */
	public void functionA() {
		Iterator it = UserDBs.entrySet().iterator();
		while (it.hasNext()) {
			HashMap.Entry pair = (HashMap.Entry) it.next();
			System.out.println(pair.getKey() + " = " + ((userModel.Utilisateur) pair.getValue()).fName + " "
					+ ((userModel.Utilisateur) pair.getValue()).lName + " " + ((userModel.Utilisateur) pair.getValue()).login);
			// avoids a ConcurrentModificationException
		}

	}

	/**
	 * Convert g.
	 */
	public void functionB() {
		Iterator it = groupes.entrySet().iterator();
		while (it.hasNext()) {
			HashMap.Entry pair = (HashMap.Entry) it.next();
			System.out.println(pair.getKey() + " = " + ((userModel.Groupe) pair.getValue()).groupeId);
			// avoids a ConcurrentModificationException
		}

	}

	public boolean loadDB() {
		try {
			File inputFile = new File("userDB.xml");
			SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(inputFile);
			Element roof;
			String login, pwd, fName, LName, id, iDGroup;
			Etudiant id_S = null;

			document = builder.build(inputFile);
			roof = document.getRootElement();
			Element roofAdmin = roof.getChild("Administrators");
			Element roofStudent = roof.getChild("Students");
			Element roofTeacher = roof.getChild("Teachers");
			Element roofGroup = roof.getChild("Groups");

			List<Element> studentList = roofStudent.getChildren();
			List<Element> adminList = roofAdmin.getChildren();
			List<Element> teacherList = roofTeacher.getChildren();
			List<Element> groupList = roofGroup.getChildren();
	
			for (int i = 0; i < studentList.size(); i++) {
				List<Element> student = studentList.get(i).getChildren();

				login = student.get(0).getText();
				fName = student.get(1).getText();
				LName = student.get(2).getText();
				pwd = student.get(3).getText();
				id = student.get(4).getText();
				iDGroup = student.get(5).getText();
				if (iDGroup.equals("-1")) {
					this.UserDBs.put(login, new Etudiant(login, id, LName, fName, pwd));
				} 	else
					this.UserDBs.put(login, new Etudiant(login, id, iDGroup, LName, fName, pwd));
			}	

				for (int i = 0; i < adminList.size(); i++) {
					List<Element> admin = adminList.get(i).getChildren();
					
					login = admin.get(0).getText();
					fName = admin.get(1).getText();
					LName = admin.get(2).getText();
					pwd = admin.get(3).getText();
					id = admin.get(4).getText();

					this.UserDBs.put(login, new Admin(login, LName, fName, id, pwd));
		}

				for (int i = 0; i < teacherList.size(); i++) {
					List<Element> teacher = teacherList.get(i).getChildren();

					login = teacher.get(0).getText();
					fName = teacher.get(1).getText();
					LName = teacher.get(2).getText();
					pwd = teacher.get(3).getText();
					id = teacher.get(4).getText();
					
					this.UserDBs.put(login, new Professeur(login, id, fName, LName, pwd));
		}
				int te = groupList.size();
				for (int i = 0; i < groupList.size(); i++) {
					List<Element> group = groupList.get(i).getChildren();

					id = group.get(0).getText();
					Groupe newGroup = new Groupe(id);
					this.groupes.put(id, newGroup);

		}
				Iterator it = UserDBs.entrySet().iterator();
		
				return true;

			} catch (Exception v0) {
			return false;
	}

}

/**
 * Save db.
 *
 * @return true, if successful
 */
	public boolean saveDB() {
		boolean bool = true;
		Object userdb = new Element("UserDB");
		org.jdom2.Document Document = new Document((Element) userdb);
		Element group = new Element("Groups");
		Element student = new Element("Students");
		Element prof = new Element("Teachers");
		Element admin = new Element("Administrators");
		((Element) userdb).addContent(group);
		((Element) userdb).addContent(student);
		((Element) userdb).addContent(prof);
		((Element) userdb).addContent(admin);
		// userdb = this.Userdb.values().iterator();
		Object Userdb1;
		Element user;
		Iterator it = groupes.entrySet().iterator();
			while (it.hasNext()) {
				HashMap.Entry pair = (HashMap.Entry) it.next();
				Element groupE = new Element("Group");
				Element groupEx = new Element("groupId").setText((String) pair.getKey());
				groupE.addContent((Content) groupEx);
				group.addContent((Content) groupE);

	}

			Iterator u = UserDBs.entrySet().iterator();
				while (u.hasNext())

	{
					HashMap.Entry pair = (HashMap.Entry) u.next();
					Element Nom;
					Element mdp;
					Element id;
					Element login;
					Element prenom;
					Userdb1 = pair.getValue();
					if (Userdb1 instanceof Etudiant) {
						Element etudiant = new Element("Student");
						Object Etudiant = (Etudiant) Userdb1;
						(login = new Element("login")).setText(((Etudiant) Etudiant).getLogin());
						(prenom = new Element("fName")).setText(((Etudiant) Etudiant).getFName());
						(Nom = new Element("lName")).setText(((Etudiant) Etudiant).getLName());
						(mdp = new Element("password")).setText(((Etudiant) Etudiant).getPassword());
				(id = new Element("iDStudent")).setText(((Etudiant) Etudiant).getIDStudent());
				Element id_Group;
				(id_Group = new Element("iDGroup")).setText(((Etudiant) Etudiant).getIDGroup());
				etudiant.addContent(login);
				etudiant.addContent(prenom);
				etudiant.addContent(Nom);
				etudiant.addContent(mdp);
				etudiant.addContent(id);
				etudiant.addContent(id_Group);
				student.addContent(etudiant);

					} else if ((Userdb1 instanceof Professeur)) {
						Element professeur = new Element("Teacher");
						Object Prof = (Professeur) Userdb1;
						(login = new Element("login")).setText(((Professeur) Prof).getLogin());
						(prenom = new Element("fName")).setText(((Professeur) Prof).getFName());
						(Nom = new Element("LName")).setText(((Professeur) Prof).getLName());
						(mdp = new Element("password")).setText(((Professeur) Prof).getPassword());
						(id = new Element("iDTeacher")).setText(((Professeur) Prof).getIDProf());
						professeur.addContent(login);
						professeur.addContent(prenom);
						professeur.addContent(Nom);
						professeur.addContent(mdp);
						professeur.addContent(id);
						prof.addContent(professeur);
						
					} else if ((Userdb1 instanceof Admin)) {
						Element administrateur = new Element("Administrator");
						Object Admin = (Admin) Userdb1;
						(login = new Element("login")).setText(((Admin) Admin).getLogin());
						(prenom = new Element("fName")).setText(((Admin) Admin).getFName());
						(Nom = new Element("LName")).setText(((Admin) Admin).getLName());
						(mdp = new Element("password")).setText(((Admin) Admin).getPassword());
						(id = new Element("iDAdmin")).setText(((Admin) Admin).Administrateur());
						administrateur.addContent(login);
						administrateur.addContent(prenom);
						administrateur.addContent(Nom);
						administrateur.addContent(mdp);
						administrateur.addContent(id);
						admin.addContent(administrateur);
					}
	}
					try

	{
						XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
						sortie.output(Document, new FileOutputStream(this.file));
		} catch (

				IOException localIOException)
					
					{
			bool = false;
	}
					return bool;
	}
}
	




