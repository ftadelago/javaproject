package userModel;
// TODO: Auto-generated Javadoc
/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/

// End of user code

/**
 * Description of Admin.
 * 
 * @author pc
 */
public class Admin extends Utilisateur {
	/**
	 * Description of the property iDAdminAdmin.
	 */
	public String adminId="0";

	/**
	 * Instantiates a new administrateur.
	 *
	 * @param login le login de l'admin
	 * @param lName le prenom
	 * @param fName le nom
	 * @param adminId l'identifiant admin
	 * @param password le mdp
	 */
	public Admin(String login, String lName, String fName, String adminId, String password) {
		super(login, lName, fName, password);
		this.adminId = adminId;
	}

	/**
	 * Administrateur.
	 *
	 * @return the string
	 */
	public final String Administrateur() {
		return this.adminId;
	}

	public final String toString() {
		String infos= "";
		infos="Administrateur :" + super.toString() + " -- " + this.adminId;
		return infos;
	}

	/**
	 * Adds the admin.
	 *
	 * @param newadlogin le login naw admin
	 * @param adminId l'identifiant admin
	 * @param fname le nopm
	 * @param srname le surnom
	 * @param password le mdp
	 * @return admin
	 */
	public Admin addAdmin(String newadlogin, String adminID, String fname, String sname, String password) {
		Admin admin = new Admin (newadlogin, sname, fname, adminID, password);
		return admin;
	}

	/**
	 * Adds the teacher.
	 *
	 * @param profLogin le login
	 * @param teacherID l'identifiant
	 * @param fname le nom
	 * @param sname le surnom
	 * @param password le mdp
	 * @return prof
	 */
	public Professeur addTeacher(String profLogin, String profId, String fname, String sname,
			String password) {
		Professeur prof = new Professeur(profLogin, profId, fname, sname, password);
		return prof;
	}

	/**
	 * Adds the student.
	 *
	 * @param etudiantLogin le login etudiant
	 * @param etutiantId le nouvel identifiant
	 * @param fname le nom
	 * @param sname le surnom
	 * @param password le mdp
	 * @return etudiant
	 */
	public Etudiant addStudent(String etudiantLogin, String etudiantId, String sname, String fname, String password) {
		Etudiant etd = new Etudiant(etudiantLogin, etudiantId, sname, fname, password);
		return etd;
	}

	/**
	 * Adds the group.
	 *
	 * @param groupeId identifiant de groupe
	 * @return le groupe 
	 */
	public Groupe addGroup(String groupeId) {
		Groupe grp = new Groupe(groupeId); 
		return grp;
	}

	/**
	 * Associate stud to group.
	 *
	 * @param etud l'etudiant
	 * @param groupeId identifiant de groupe
	 * @param A un groupe
	 */
	public void associateStudToGroup(Etudiant etud, String groupId, Groupe A) {
		etud.setIDGroup(groupId);
		A.addStudent(etud);

	}


	@Override
	public String check() {
		String checker = "Admin";
		return checker;
	}
}
