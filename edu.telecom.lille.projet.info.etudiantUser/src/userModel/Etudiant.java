package userModel;

/**
 * Description of Student.
 * 
 * @author pc
 */
public class Etudiant extends Utilisateur {
	/**
	 * Description of the property etudiantId.
	 */
	private String etudiantId;

	/**
	 * Description of the property groupeId.
	 */
	private String groupeId="0";
	// Start of user code (user defined attributes for Student)

	/**
	 * Instantiates a new student.
	 *
	 * @param login the login
	 * @param etudiantId the i d student
	 * @param lName the last name
	 * @param fName the first name
	 * @param password the password
	 */
	// End of user code
	public Etudiant(String login, String etudiantId, String fName, String lName, String password) {
		// Start of user code constructor for Student)
		super(login, password, lName, fName);
		this.etudiantId = etudiantId;

		// End of user code
	}

	/**
	 * Instantiates a new student.
	 *
	 * @param login the login
	 * @param etudiantId the i d student
	 * @param groupeId the i d group
	 * @param password the password
	 * @param lName the last name
	 * @param fName the first name	 
	 */
	public Etudiant(String login, String lName, String fName, String etudiantId, String groupeId, String password) {
		// Start of user code constructor for Student)
		super(login, password, lName, fName);
		this.groupeId = groupeId;
		this.etudiantId = etudiantId;
		
		// End of user code
	}


	public final String toString() {
		String infos = "";
		infos="Etudiant:" + super.toString() + " \n " + this.etudiantId + " \n " + this.groupeId;
		return infos;
	}


	@Override
	public String check() {
		return "Etudiant";
	}

	/**
	 * Gets the ID student.
	 *
	 * @return the ID student
	 */
	public String getIDStudent() {
		String getId = this.etudiantId;
		return getId;
	}

	/**
	 * Sets a value to attribute etudiantId.
	 *
	 * @param newID the new ID student
	 */
	public void setIDStudent(String newID) {
		this.etudiantId = newID;
	}

	/**
	 * Returns groupeId.
	 * 
	 * @return groupeId
	 */
	public String getIDGroup() {
		return this.groupeId;
	}

	/**
	 * Sets a value to attribute groupId.
	 *
	 * @param newgrpId the new ID group
	 */
	public void setIDGroup(String newgrpId) {
		this.groupeId = newgrpId;
	}

}
