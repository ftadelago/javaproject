package userModel;

import java.util.HashSet;

// TODO: Auto-generated Javadoc
/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/

// Start of user code (user defined imports)

// End of user code

/**
 * Description of Group.
 * 
 * @author pc
 */
public class Groupe {

	/** Les membres */
	public HashSet<Etudiant> membres;
	
	/** Effectif du groupe */
	private int effectif = 0;
	
	/** l'iD du groupe */
	public String groupeId;

	/**
	 * The constructor.
	 *
	 * @param newgroupId l'Id du nouveau groupe
	 */
	public Groupe(String newgroupId) {
		
		this.membres = new HashSet<Etudiant>();		
		this.effectif = 0;
		this.groupeId = newgroupId;

	}

	/**
	 * Adds student.
	 *
	 * @param etud le nouvel adh�rent
	 */
	public void addStudent(Etudiant etud) {
		if (!membres.contains(etud)) {		
			effectif=effectif+1 ;
			membres.add(etud);
		}
	}

	/**
	 * Removes the student.
	 *
	 * @param etud l'etudiant a retirer
	 */
	public void removeStudent(Etudiant etud) {
		if (membres.contains(etud)) {
			effectif=effectif-1;
			membres.remove(etud);
		}
	}

	/**
	 * Groupe.
	 *
	 * @return the string
	 */
	public String Group() {
		return this.groupeId;
	}

	/**
	 * Gets number of students
	 *
	 * @return effectif
	 */
	public int getStudNbr() {
		return this.effectif;
	}

	/**
	 * Gets the students.
	 *
	 * @return membres
	 */
	public HashSet<Etudiant> getStudents() {
		return this.membres;
	}

	
	public String toString() {
		String groupe ="Id: " + groupeId + "\n" + "Effectif: " + effectif + "\n" + "Membres du groupe: " + "/n" ;
		String log,iD;
		for (Etudiant etd : membres) {
			log=etd.getLogin();
			iD=etd.getIDStudent();
			groupe = groupe + "Login: " + log + " -- ID: " + iD + "\n";
		}
		return groupe;
	}
}
