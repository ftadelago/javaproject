package userModel;

import java.util.HashSet;


//Start of user code (user defined imports)

//End of user code

/**
* Description of User.
* 
* @author pc
*/
public class Utilisateur {

	/**
	 * Description of the property userDBs.
	 */
	public HashSet<UserDB> userDataBase = new HashSet<UserDB>();

	/**
	 * Description of the property login.
	 */
	public String login;

	/**
	 * Description of the property fName.
	 */
	public String fName;

	/**
	 * Description of the property lName.
	 */
	public String lName;

	/**
	 * Description of the property password.
	 */
	protected String password;

	// Start of user code (user defined attributes for User)

	// End of user code

	/**
	 * Instantiates a new user.
	 *
	 * @param login the login
	 * @param password the password
	 * @param lName the l name
	 * @param fName the f name
	 */
	public Utilisateur (String login, String password, String lName, String fName) {

		this.login = login;
		this.fName = fName;
		this.lName = lName;
		this.password = password;

	}
	// End of user code

	/**
	 * Description of the method check.
	 *
	 * @return the string
	 */
	public String check() {
		return null;
		// Start of user code for method check
		// End of user code
	}

	// Start of user code (user defined methods for User)

	// End of user code
	/**
	 * Returns userDBs.
	 * 
	 * @return userDBs
	 */
	public HashSet<UserDB> getUserDBs() {
		return this.userDataBase;
	}

	/**
	 * Returns login.
	 * 
	 * @return login
	 */
	public String getLogin() {
		return this.login;
	}

	/**
	 * Sets a value to attribute login.
	 *
	 * @param newLogin the new login
	 */
	public void setLogin(String newLogin) {
		this.login = newLogin;
	}

	/**
	 * Returns fName.
	 * 
	 * @return fName
	 */
	public String getFName() {
		return this.fName;
	}

	/**
	 * Sets a value to attribute fName.
	 *
	 * @param newFName the new f name
	 */
	public void setFName(String newFName) {
		this.fName = newFName;
	}

	/**
	 * Returns lName.
	 * 
	 * @return lName
	 */
	public String getLName() {
		return this.lName;
	}

	/**
	 * Sets a value to attribute lName.
	 *
	 * @param newLName the new l name
	 */
	public void setLName(String newLName) {
		this.lName = newLName;
	}

	/**
	 * Returns password.
	 * 
	 * @return password
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * Sets a value to attribute password.
	 *
	 * @param newPassword the new password
	 */
	public void setPassword(String newPassword) {
		this.password = newPassword;
	}


}
