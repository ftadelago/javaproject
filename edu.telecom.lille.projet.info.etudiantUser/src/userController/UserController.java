package userController;

import java.util.HashMap;
import java.util.Iterator;

import userModel.Admin;
import userModel.Etudiant;
import userModel.Groupe;
import userModel.Professeur;
import userModel.UserDB;
import userModel.Utilisateur;
/**
 * Cette classe est le contrôleur d'utilisateurs que vous devez implémenter. 
 * Elle contient un attribut correspondant à la base de données utilisateurs que vous allez créer.
 * Elle contient toutes les fonctions de l'interface IUserController que vous devez implémenter.
 * 
 * @author Jose Mennesson (Mettre à jour)
 * @version 04/2016 (Mettre à jour)
 * 
 */

//TODO Classe à modifier

public class UserController implements IUserController
{
	
	/**
	 * Contient une instance de base de données d'utilisateurs
	 * 
	 */
	private HashMap<String, Groupe> grpHash;
	private HashMap<String, Utilisateur> userHash;
	
	private UserDB userDb;
	
	
	/**
	 * Constructeur de controleur d'utilisateurs créant la base de données d'utilisateurs
	 * 
	 * @param userfile
	 * 		Fichier XML contenant la base de données d'utilisateurs
	 */
	public UserController(String userfile){
		UserDB userDB=new UserDB(userfile);
		this.setUserDB(userDB);
		loadDB();
		
		Iterator it2 = userHash.entrySet().iterator();
		while (it2.hasNext()) {
			HashMap.Entry pair2 = (HashMap.Entry) it2.next();
			String type2 = pair2.getValue().getClass().getName();
			if (type2.equals("userModel.Student")) {
				if (!((userModel.Etudiant) pair2.getValue()).getIDGroup().equals("-1")) {
					grpHash.get(((userModel.Etudiant) pair2.getValue()).getIDGroup())
							.addStudent(((userModel.Etudiant) pair2.getValue()));
				}
			}
		}

	}


	@Override
	public String getUserName(String userLogin) {
		String name = null;
		if (userHash.containsKey(userLogin)){
			name="Utilisateur: " + userHash.get(userLogin). getFName()+""+ userHash.get(userLogin).getLName();
		}
		// TODO Auzto-generated method stub
		return name;
	}
	 	

	@Override
	public String getUserClass(String userLogin, String userPwd) {
		// TODO Auto-generated method stub
		String std;
		if (userHash.containsKey(userLogin)) {
			std=userHash.get(userLogin).getPassword();
			if (std==userPwd) {
				return userHash.get(userLogin).check();
			}
		}
		return "Erreur: V�rifier userLogin et userPwd";
	}

	@Override
	public int getStudentGroup(String studentLogin) {
		// TODO Auto-generated method stub
		Integer grpId=-1;
		if(userHash.containsKey(studentLogin)){
			grpId=Integer.parseInt(((userModel.Etudiant)userHash.get(studentLogin)).getIDGroup());
		}
		return grpId;
	}

	@Override
	public boolean addAdmin(String adminLogin, String newAdminlogin, int adminID, String firstname, String surname,
			String pwd) {
		// TODO Auto-generated method stub
		boolean added= false;
		if(userHash.containsKey(adminLogin)){
			if (!userHash.containsKey(newAdminlogin)){
				userHash.put(newAdminlogin, ((userModel.Admin) userHash.get(adminLogin)).addAdmin(newAdminlogin,
						"--" + adminID, firstname, surname, pwd));
				userDb.putUtilisateurs(userHash);
				saveDB();
				added=true;
			}
			}
		return added;
	}

	@Override
	public boolean addTeacher(String adminLogin, String newteacherLogin, int teacherID, String firstname,
			String surname, String pwd) {
		// TODO Auto-generated method stub
		boolean added= false;
		if (userHash.containsKey(adminLogin) ) {
			if (!userHash.containsKey(newteacherLogin)){
			userHash.put(newteacherLogin, ((userModel.Admin) userHash.get(adminLogin)).addTeacher(newteacherLogin,
					"--" + teacherID, surname, firstname, pwd));
			userDb.putUtilisateurs(userHash);
			saveDB();
			added=true;
			}
		}	
		return added;
	}

	@Override
	public boolean addStudent(String adminLogin, String newStudentLogin, int studentID, String firstname,
			String surname, String pwd) {
		// TODO Auto-generated method stub
		boolean added = false;
		if (userHash.containsKey(adminLogin)) {
			if (!userHash.containsKey(newStudentLogin))
			userHash.put(newStudentLogin, ((userModel.Admin) userHash.get(adminLogin)).addStudent(newStudentLogin,
					"--" + studentID, surname, firstname, pwd));
			userDb.putUtilisateurs(userHash);
			saveDB();
			added=true;
		}
		return added;
	}
	public boolean removeUser(String adminLogin, String userLogin) {
		boolean added= false;
		if (userHash.containsKey(adminLogin) && userHash.containsKey(userLogin)) {
			if (userHash.get(userLogin).check().equals("Student")) {
				String gid = (String) ((userModel.Etudiant) userHash.get(userLogin)).getIDGroup();
				if (!gid.equals("-1")) {
					grpHash.get(gid).removeStudent(((userModel.Etudiant) userHash.get(userLogin)));
				}
			}
			userHash.remove(userLogin);
			added=true;
		}
		return added;
	}

	@Override
	public boolean removeGroup(String adminLogin, int groupId) {
		boolean added= false;
		if (userHash.containsKey(adminLogin) && grpHash.containsKey("" + groupId)) {
			Iterator it = userHash.entrySet().iterator();
			while (it.hasNext()) {
				HashMap.Entry pair = (HashMap.Entry) it.next();
				String type = pair.getValue().getClass().getName();
				if (type.equals("userModel.Student")) {
					if (((userModel.Etudiant) pair.getValue()).getIDGroup().equals("" + groupId)) {
						((userModel.Etudiant) pair.getValue()).setIDGroup("-1");
					}
				}
			}
			grpHash.remove(""+groupId);
			added=true;
		}
		return added;
	}

	public boolean addGroup(String adminLogin, int groupId, String stdlogin) {
		boolean added= false;
		if (userHash.containsKey(adminLogin) && !userHash.containsKey("" + groupId)) {
			grpHash.put("" + groupId, new Groupe("" + groupId));
			associateStudToGroup(adminLogin, stdlogin, groupId);
			added=true;
		}
		return added;
	}

	@Override
	public boolean associateStudToGroup(String adminLogin, String studentLogin, int groupId) {
		boolean added= false;
		if (userHash.containsKey(adminLogin) && grpHash.containsKey("" + groupId) && userHash.containsKey(studentLogin)) {
			((userModel.Admin) userHash.get(adminLogin)).associateStudToGroup(
					((userModel.Etudiant) userHash.get(studentLogin)), "" + groupId, grpHash.get("" + groupId));
			added=true;
		}

		return added;
	}

	@Override
	public String[] usersToString() {
		Iterator it = userHash.entrySet().iterator();
		String[] users = new String[userHash.size()];
		int i = 0;
		while (it.hasNext()) {
			HashMap.Entry pair = (HashMap.Entry) it.next();
			//System.out.println(pair.getValue().getClass().getName());
			Utilisateur u = ((userModel.Utilisateur) pair.getValue());
			String type = pair.getValue().getClass().getName();
			if (type.equals("userModel.Etudiant")) {
				Etudiant s = ((userModel.Etudiant) pair.getValue());
				users[i] = " Type:" + s.check();
				users[i] += " Name:" + s.getFName() + "--" + s.getLName();
				users[i] += " etudiantId:" + s.getIDStudent();
				users[i] += " GroupID:" + s.getIDGroup();
				users[i] += " Login:" + s.getLogin();
				users[i] += " password:" + s.getPassword();
			} else if (type.equals("userModel.Prof:")) {
				Professeur s = ((userModel.Professeur) pair.getValue());
				users[i] = " Type:" + s.check();
				users[i] += " Name:" + s.getFName() + "--" + s.getLName();
				users[i] += " profID:" + s.getIDProf();
				users[i] += " Login:" + s.getLogin();
				users[i] += " password:" + s.getPassword();
			} else if (type.equals("userModel.Admin")) {
				Admin s = ((userModel.Admin) pair.getValue());
				users[i] = "Type:" + s.check();
				users[i] += "Name:" + s.getFName() + "--" + s.getLName();
				users[i] += "Login:" + s.getLogin();
				users[i] += "password:" + s.getPassword();
			}
			i++;
		}
		return users;
	}

	@Override
	public String[] usersLoginToString() {
		Iterator it = userHash.entrySet().iterator();
		String[] users = new String[userHash.size()];
		int i = 0;
		while (it.hasNext()) {
			HashMap.Entry pair = (HashMap.Entry) it.next();
			users[i] = (String) pair.getKey();
			i++;
		}
		return users;
	}

	@Override
	public String[] studentsLoginToString() {
		Iterator it = userHash.entrySet().iterator();
		String[] users = new String[userHash.size()];
		int i = 0;
		while (it.hasNext()) {
			HashMap.Entry pair = (HashMap.Entry) it.next();
			String type = pair.getValue().getClass().getName();
			if (type.equals("userModel.Etudiant")) {
				users[i] = (String) pair.getKey();
				i++;
			}
		}
		return users;
	}

	@Override
	public String[] groupsIdToString() {
		Iterator it = grpHash.entrySet().iterator();
		String[] users = new String[grpHash.size()];
		int i = 0;
		while (it.hasNext()) {
			HashMap.Entry pair = (HashMap.Entry) it.next();
			users[i] = (String) pair.getKey();
			i++;

		}
		return users;

	}

	@Override
	public String[] groupsToString() {
		Iterator it = grpHash.entrySet().iterator();
		String[] users = new String[grpHash.size()];
		int i = 0;
		while (it.hasNext()) {
			HashMap.Entry pair = (HashMap.Entry) it.next();
			users[i] = ((userModel.Groupe) pair.getValue()).toString();
			i++;
		}
		return users;
	}

	@Override
	public boolean loadDB() {
		// TODO Auto-generated method stub
		boolean loaded =false;
		try {
			userDb.loadDB();
			userHash = userDb.UserDBHash();
			grpHash = userDb.GroupHash();
			loaded = true;
		} catch (Exception v0) {
			loaded =false;
		}
		return loaded;
	}

	@Override
	public boolean saveDB() {
		// TODO Auto-generated method stub
		try {
			userDb.saveDB();
			return true;
		} catch (Exception v0) {
			return false;
		}
	}
	public UserDB getUserDB() {
		return userDb;
	}

	public void setUserDB(UserDB userDB) {
		this.userDb = userDB;
	}

	@Override
	public boolean addGroup(String adminLogin, int groupId) {
		// TODO Auto-generated method stub
		return false;
	}
	
	

}

