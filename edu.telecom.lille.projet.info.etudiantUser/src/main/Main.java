package main;

import java.io.IOException;
import java.util.Arrays;

import org.jdom2.JDOMException;

import timeTableController.TimeTableController;
import userController.UserController;
import view.MainFrame;
/**
 * Cette classe est la classe principale de l'application. 
 * Elle crée les contrôleurs d'utilisateurs et d'emplois de temps ainsi que l'interface graphique (MainFrame).
 * 
 * @author Jose Mennesson
 * @version 04/2016
 */

//TODO Classe à ne pas modifier

public class Main {
	
	/**
	 * Crée l'interface graphique
	 * 
	 * @param userController
	 * 				Le controleur des utilisateurs
	 * 
	 * @param tTController
	 * 				Le controleur d'emplois du temps
	 */
	private static void createAndShowUI(UserController userController,TimeTableController tTController) {
		new MainFrame(userController,tTController);
	}
	
	/**
	 * Fonction principale du programme
	 * 
	 * @param args
	 * 		Arguments donnés en entrée du programme 
	 * 
	 */
	public static void main(String[] args) throws JDOMException, IOException {
		final String file = "userDBs.xml";
		UserController control = new UserController(file);
		System.out.println("Utd UserName :" + control.getUserName("Utd"));
		System.out.println("Utd est :" + control.getUserClass("Utd", "prout") + " Utilisateur");
		System.out.println("Utd appartient a :" + control.getStudentGroup("Utd") + " Group si -1 alors l'etudiant n'a pas encore de groupe");

		control.removeUser("su", "BS");
		System.out.println("Group IDs: " + Arrays.toString(control.groupsIdToString()));
		System.out.println("Infos Groups: " + Arrays.toString(control.groupsToString()));
		System.out.println("Ajouter groupe avec groupeID = 0000 et mettre Utd dans ce groupe" + control.addGroup("su", 0000, "Utd"));
		control.removeGroup("su", 0000);
		control.associateStudToGroup("su", "Utd", 1);
		System.out.println("Infos Utilisateurs : " + Arrays.toString(control.usersToString()));
		System.out.println("User Logins: " + Arrays.toString(control.usersLoginToString()));
		System.out.println("Etudiants Logins: " + Arrays.toString(control.studentsLoginToString()));
		System.out.println("Group Logins: " + Arrays.toString(control.groupsIdToString()));
		System.out.println("Group Logins: " + Arrays.toString(control.groupsToString()));
		// UC.addAdmin("su", "NT", 2017, "Nicolas", "TADE", "@tron");
		// UC.addTeacher("su", "JM", 2005, "Jose", "Menesson",
		// "salsepareille");
		// UC.addStudent("su", "ST", 2000, "StdTest", "TEST", "stake");

		control.saveDB();

	}
}
